''' <summary> Selects a test panel. </summary>
''' <remarks> Launch this form by calling its Show or ShowDialog method from its default instance. </remarks>
''' <license> (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="2/15/2014" by="David" revision=""> Documented. </history>
Public Class Switchboard
    Inherits isr.Core.Diagnosis.UserFormBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    Public Sub New()
        MyBase.New()

        ' Initialize user components that might be affected by resize or paint actions

        ' This method is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call

    End Sub

#Region " UNUSED "
#If False Then
  ''' <summary>Cleans up managed components.</summary>
  ''' <remarks>Use this method to reclaim managed resources used by this class.</remarks>
  Private Sub onDisposeManagedResources()
  End Sub

  ''' <summary>Cleans up unmanaged components.</summary>
  ''' <remarks>Use this method to reclaim unmanaged resources used by this class.</remarks>
  Private Sub onDisposeUnmanagedResources()
  End Sub

#End If
#End Region

#End Region

#Region " PROPERTIES "

    ''' <summary>Gets or sets the status message.</summary>
    Private Property statusMessage As String = String.Empty

#End Region

#Region " PRIVATE  and  PROTECTED "

    ''' <summary>
    ''' Enumerates the available test panels.
    ''' </summary>
    Private Enum TestPanel
        <System.ComponentModel.Description("Not selected")> None
        <System.ComponentModel.Description("Reports")> Reports
    End Enum

    ''' <summary>
    ''' Initializes the user interface and tool tips.
    ''' </summary>
    ''' <remarks>Call this method from the form load method to set the user interface.</remarks>
    Private Sub initializeUserInterface()
        Me._PanelComboBox.Items.Clear()
        ' Me.panelComboBox.DataSource = [Enum].GetNames(GetType(TestPanel))
        Me._PanelComboBox.DataSource = isr.Core.Primitives.EnumExtensions.ValueDescriptionPairs(GetType(TestPanel))
        Me._PanelComboBox.DisplayMember = "Value"
        Me._PanelComboBox.ValueMember = "Key"

    End Sub

#Region " UNUSED "
#If False Then

  ''' <summary>Initializes the class objects.</summary>
  ''' <exception cref="isr.Drawing.BaseException" guarantee="strong">
  '''   failed instantiating objects.</exception>
  ''' <remarks>Called from the form load method to instantiate 
  '''   module-level objects.</remarks>
  Private Sub instantiateObjects()

  End Sub

  ''' <summary>Terminates and disposes of class-level objects.</summary>
  ''' <remarks>Called from the form Closing method.</remarks>
  Private Sub terminateObjects()

  End Sub

#End If
#End Region

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary>Occurs before the form is closed</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.ComponentModel.CancelEventArgs"/></param>
    ''' <remarks>Use this method to optionally cancel the closing of the form.
    ''' Because the form is not yet closed at this point, this is also the best 
    ''' place to serialize a form's visible properties, such as size and 
    ''' location. Finally, dispose of any form level objects especially those that
    ''' might needs access to the form and thus should not be terminated after the
    ''' form closed.
    ''' </remarks>
    Private Sub form_Closing(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

        ' disable the timer if any
        ' actionTimer.Enabled = False
        My.Application.DoEvents()

        ' set module objects that reference other objects to Nothing

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try
            ' terminate form-level objects
            ' Me.terminateObjects()
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try

    End Sub

    ''' <summary>Occurs when the form is loaded.</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
    ''' <remarks>Use this method for doing any final initialization right before 
    '''   the form is shown.  This is a good place to change the Visible and
    '''   ShowInTaskbar properties to start the form as hidden.  
    '''   Starting a form as hidden is useful for forms that need to be running but that
    '''   should not show themselves right away, such as forms with a notify icon in the
    '''   task bar.</remarks>
    Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' instantiate form objects
            'Me.instantiateObjects()

            ' set the form caption
            Me.Text = My.Application.Info.BuildDefaultCaption(": SWITCHBOARD")

            ' set tool tips
            initializeUserInterface()

            ' center the form
            Me.CenterToScreen()

            ' turn on the loaded flag
            '      loaded = True

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    Private Sub Tester_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        Me._MessagesList.AddMessage("Activated")
    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary>Closes the form and exits the application.</summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub exitButton_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles _ExitButton.Click

        Try

            Me._StatusLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                 "{0} {1}", Date.Now.Ticks, CommandLineInfo.DevicesEnabled.GetValueOrDefault(True).ToString)
            Me.Close()

        Catch ex As Exception

            ' report failure 
            Me.statusMessage = String.Format(Globalization.CultureInfo.CurrentCulture,
                                           "Failed closing {0}{1}", Environment.NewLine, ex.Message)
            MessageBox.Show(Me.statusMessage, Me.Name, MessageBoxButtons.OK, MessageBoxIcon.Error,
                            MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)

        End Try

    End Sub

    Private Sub callOffButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _CancelButton.Click
        Me.Close()
    End Sub

    ''' <summary>
    ''' Opens the selected form.
    ''' </summary>
    Private Sub openButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _OpenButton.Click
        Dim selectedKeyValuePair As System.Collections.Generic.KeyValuePair(Of [Enum], String)
        selectedKeyValuePair = CType(Me._PanelComboBox.SelectedItem, System.Collections.Generic.KeyValuePair(Of [Enum], String))
        Select Case CType(selectedKeyValuePair.Key, TestPanel)
            Case TestPanel.Reports
                Using r As New ReportTestPanel
                    r.ShowDialog()
                End Using
        End Select
    End Sub

#End Region

End Class