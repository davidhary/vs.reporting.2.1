Public Class Person

  Private mName As String
  Private mCity As String
  Private mState As String

  Public Sub New(ByVal name As String, ByVal city As String, ByVal state As String)
    mName = Name
    mCity = city
    mState = state
  End Sub

  Public Property Name() As String
    Get
      Return mName
    End Get
    Set(ByVal value As String)
      mName = value
    End Set
  End Property

  Public Property City() As String
    Get
      Return mCity
    End Get
    Set(ByVal value As String)
      mCity = value
    End Set
  End Property

  Public Property State() As String
    Get
      Return mState
    End Get
    Set(ByVal value As String)
      mState = value
    End Set
  End Property

End Class
