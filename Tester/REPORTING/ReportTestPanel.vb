Imports isr.Drawing.Reporting

''' <summary> Uses a chart to test reporting. </summary>
''' <remarks> Launch this form by calling its Show or ShowDialog method from its default instance. </remarks>
''' <license> (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="2/15/2014" by="David" revision=""> Reported. </history>
Public Class ReportTestPanel
    Inherits isr.Core.Diagnosis.UserFormBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    Public Sub New()
        MyBase.New()

        ' instantiate objects that depend on resize.

        ' This method is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call

    End Sub


#End Region

#Region " PRIVATE  and  PROTECTED "

    ''' <summary>Gets or sets the status message.</summary>
    Private Property statusMessage As String = String.Empty

    Private Property graphicsBuffer As isr.Drawing.BufferedGraphics
    Private Property chartPane As isr.Drawing.ChartPane
    ''' <summary>
    ''' Creates the chart.
    ''' </summary>
    Private Sub CreateChart()
        Me.graphicsBuffer = New isr.Drawing.BufferedGraphics(Me.ClientRectangle.Width, Me.ClientRectangle.Height)
        Me.chartPane = New isr.Drawing.ChartPane
        Dim rec As New System.Drawing.Rectangle(Me._MessagesList.Location, Me._MessagesList.Size)
        Me.chartPane.CreateSampleOne(rec)
    End Sub

    ''' <summary>
    ''' Initializes the user interface and tool tips.
    ''' </summary>
    ''' <remarks>Call this method from the form load method to set the user interface.</remarks>
    Private Sub initializeUserInterface()

        ' set the combo box
        With Me._ReportOptionsComboBox
            .Items.Clear()
            .Items.Add("select a report option:")
            .Items.Add("Report coded in the program")
            .Items.Add("Report coded in the program with auto paging")
            .Items.Add("Report from object data.")
            .Items.Add("Multi-Section report.")
            .Items.Add("Report from SQL data set.")
            .Items.Add("Report from SQL and object data.")
            .SelectedIndex = 0
        End With

        CreateChart()

    End Sub

    ''' <summary>
    ''' Terminates and disposes of class-level objects.
    ''' </summary>
    ''' <remarks>Called from the form Closing method.</remarks>
    Private Sub terminateObjects()

        If Me._graphicsBuffer IsNot Nothing Then
            Me.graphicsBuffer.Dispose()
            Me._graphicsBuffer = Nothing
        End If
        If Me._chartPane IsNot Nothing Then
            Me._chartPane.Dispose()
            Me._chartPane = Nothing
        End If

    End Sub

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary>
    ''' Occurs when the form is loaded.
    ''' </summary>
    ''' <param name="sender"><see cref="System.Object" /> instance of this
    '''   <see cref="System.Windows.Forms.Form" /></param>
    ''' <param name="e"><see cref="System.EventArgs" /></param>
    ''' <remarks>Use this method for doing any final initialization right before
    ''' the form is shown.  This is a good place to change the Visible and
    ''' ShowInTaskbar properties to start the form as hidden.
    ''' Starting a form as hidden is useful for forms that need to be running but that
    ''' should not show themselves right away, such as forms with a notify icon in the
    ''' task bar.</remarks>
    Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' set the form caption
            Me.Text = My.Application.Info.BuildDefaultCaption(": REPORT TEST PANEL")

            ' set tool tips
            Me.initializeUserInterface()

            ' center the form
            Me.CenterToScreen()

            ' turn on the loaded flag
            '        loaded = True

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            ' Turn off the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary>
    ''' Paints the chart.
    ''' </summary>
    ''' <param name="parent">The parent.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.PaintEventArgs" /> instance containing the event data.</param>
    Private Sub PaintChart(ByVal parent As Control, e As System.Windows.Forms.PaintEventArgs)

        If Not IsDisposed AndAlso Not parent Is Nothing Then
            If Me.graphicsBuffer.CanDoubleBuffer() Then

                Using brush As New SolidBrush(SystemColors.Window)
                    ' Fill in Background (for efficiency only the area that has been clipped)
                    ' Me.GraphicsBuffer.GraphicsDevice.FillRectangle(brush, e.ClipRectangle.X, e.ClipRectangle.Y, e.ClipRectangle.Width, e.ClipRectangle.Height)
                    Me.graphicsBuffer.GraphicsDevice.FillRectangle(brush, e.ClipRectangle)
                End Using

                Using brush As New SolidBrush(Color.Gray)
                    ' clear the client area
                    Me.graphicsBuffer.GraphicsDevice.FillRectangle(brush, parent.ClientRectangle)
                End Using

                ' Do our drawing using Me._graphicsBuffer.g instead e.Graphics
                Me.chartPane.Draw(Me._graphicsBuffer.GraphicsDevice)

                ' Render to the control.
                Me.graphicsBuffer.Render(e.Graphics)

            Else
                ' if double buffer is not available, draw to e.Graphics

                ' clear
                Using brush As New SolidBrush(Color.Gray)
                    e.Graphics.FillRectangle(brush, parent.ClientRectangle)
                End Using

                ' draw
                Me._chartPane.Draw(e.Graphics)

            End If

        End If

    End Sub

    ''' <summary>
    ''' Handles the Paint event of the _ChartTabPage control.
    ''' Updates the chart.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.PaintEventArgs" /> instance containing the event data.</param>
    Private Sub _ChartTabPage_Paint(sender As Object, e As System.Windows.Forms.PaintEventArgs) Handles _ChartTabPage.Paint
        If Not IsDisposed AndAlso Not sender Is Nothing Then
            PaintChart(CType(sender, Control), e)
        End If
    End Sub

    ''' <summary>
    ''' Resizes the chart.
    ''' </summary>
    ''' <param name="parent">The parent.</param>
    Private Sub ResizeChart(ByVal parent As Control)
        If Not IsDisposed AndAlso Not parent Is Nothing Then
            If Me._graphicsBuffer IsNot Nothing Then
                Me._graphicsBuffer.CreateDoubleBuffer(parent.ClientRectangle.Width, parent.ClientRectangle.Height)
            End If
            If Me._chartPane IsNot Nothing Then
                Me._chartPane.SetSize(parent.ClientRectangle)
            End If
            parent.Invalidate()
        End If
    End Sub

    ''' <summary>
    ''' Handles the Resize event of the _ChartTabPage control.
    ''' Resizes the chart.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub _ChartTabPage_Resize(sender As Object, e As System.EventArgs) Handles _ChartTabPage.Resize
        If Not IsDisposed AndAlso Not sender Is Nothing Then
            ResizeChart(CType(sender, Control))
        End If
    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary>Closes the form and exits the application.</summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub exitButton_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles _ExitButton.Click

        Try

            Me._StatusLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                 "{0} {1}", Date.Now.Ticks, CommandLineInfo.DevicesEnabled.GetValueOrDefault(True).ToString)
            Me.Close()

        Catch ex As Exception

            ' report failure 
            Me.statusMessage = String.Format(Globalization.CultureInfo.CurrentCulture,
                                           "Failed closing {0}{1}", Environment.NewLine, ex.Message)
            MessageBox.Show(Me.statusMessage, Me.Name, MessageBoxButtons.OK, MessageBoxIcon.Error,
                            MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)

        End Try

    End Sub

    Private Sub callOffButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _CancelButton.Click
        Me.Close()
    End Sub

    Private Sub testButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _TestButton.Click

        ' check if a report option was selected
        If Me._ReportOptionsComboBox.SelectedIndex > 0 Then

            ' show report option selected
            Me._MessagesList.AddMessage("Generating report: " & Me._ReportOptionsComboBox.Text)

            Select Case Me._ReportOptionsComboBox.SelectedIndex

                Case 0

                Case 1

                    Me.doReportFromCode()

                Case 2

                    doReportAutoPaging()

                Case 3

                    doReportFromObjectArrayList()

                Case 4

                    Me.doMultiPartReport()

                Case 5

                    doReportFromDataSet()

                Case 6

                    Me.doReportFromultiPartReportDataSourcesb()

                Case Else
                    Debug.Assert(Not Debugger.IsAttached, "Unhandled report option")
            End Select

        End If

    End Sub

#End Region

#Region " REPORT FROM CODE "

    Private WithEvents codedReport As isr.Drawing.Reporting.ReportDocument

    Private Sub doReportFromCode()

        codedReport = New isr.Drawing.Reporting.ReportDocument
        With codedReport

            With .AddHeading(Date.Now.ToString(Globalization.CultureInfo.CurrentCulture), isr.Drawing.Reporting.HeadingType.SupHeader)
                .Appearance.Justification = isr.Drawing.Reporting.LineJustification.Left
            End With

            With .AddHeading("ISR Report Generator", isr.Drawing.Reporting.HeadingType.SupHeader)
                .Appearance.Justification = isr.Drawing.Reporting.LineJustification.Right
            End With

            With .AddHeading("Report from code (program generated)", isr.Drawing.Reporting.HeadingType.Header)
                .Appearance.Font = New Font(.Appearance.Font, FontStyle.Bold)
            End With

            With .AddHeading("Page {0} of {1}", HeadingType.SupFooter)
                .Appearance.Justification = LineJustification.Right
                .IsPage = True
                .IsPageCount = True
                .Visible = True
            End With

            With .AddHeading("(c) 2010 Integrated Scientific Resources, Inc., Confidential", HeadingType.Footer)
                .Appearance.Justification = LineJustification.Left
            End With

            .Font = New Font("Ariel", 10)

            ' we have two pages
            .PageCount = 2

            ' print preview
            .Print(True)

        End With

    End Sub

    ''' <summary>This event is raised for each page immediately after the header for 
    ''' the page has been printed. The cursor is on the first line of the report body.</summary>
    ''' <remarks>Use this method to print the body of the report.</remarks>
    Private Sub codedReport_PrintPageBodyStart(ByVal sender As Object,
                                               ByVal e As isr.Drawing.Reporting.ReportPageEventArgs) Handles codedReport.PrintPageBodyStarted

        Dim reportDoc As isr.Drawing.Reporting.ReportDocument = CType(sender, isr.Drawing.Reporting.ReportDocument)

        With e
            If e.PageNumber = 1 Then

                .WriteLine("Welcome to our report.")
                .WriteLine()
                .Write("We can print some text, and ")
                .WriteLine("then put some more text on the same line.")
                .WriteLine("This works much like writing to the console.")
                .WriteLine()
                .WriteLine()
                .Write("We can left-justify", LineJustification.Left)
                .Write("We can also center text", LineJustification.Centered)
                .WriteLine("And we can right-justify", LineJustification.Right)
                .WriteLine()
                .WriteLine()
                .WriteLine("Much simpler than doing this all by hand.")
                .HasMorePages = reportDoc.PageCount > e.PageNumber

            Else

                .WriteLine("This is page 2.")
                .WriteLine("Just to show that we handle multiple pages.")

                ' add printing of graph
                Me._chartPane.Print(e.Graphics, e.PageSettings.Bounds) '  New RectangleF(72, 288, 72, 288))
                '          Me._chartPane.Print(e.Graphics, e.PageSettings.Bounds, New Rectangle(72, 288, 72, 288))
                .HasMorePages = reportDoc.PageCount > e.PageNumber

            End If

        End With

    End Sub

    Private Sub codedReport_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles codedReport.PrintPage

    End Sub

#End Region

#Region " REPORT WITH AUTO PAGING "

    Private WithEvents autoPageReport As isr.Drawing.Reporting.ReportDocument
    Private consecutiveLineNumber As Int32
    Private Const linesToPrint As Int32 = 200

    Private Sub doReportAutoPaging()
        Using autoPageReport As New isr.Drawing.Reporting.ReportDocument
            With autoPageReport

                With .AddHeading(Date.Now.ToString(Globalization.CultureInfo.CurrentCulture), HeadingType.SupHeader)
                    .Appearance.Justification = LineJustification.Left
                End With

                With .AddHeading("ISR Report Generator", HeadingType.SupHeader)
                    .Appearance.Justification = LineJustification.Right
                End With

                With .AddHeading("Program generated report with auto paging", HeadingType.Header)
                    .Appearance.Font = New Font(.Appearance.Font, FontStyle.Bold)
                End With

                With .AddHeading("(c) 2010 Integrated Scientific Resources, Inc., Confidential", HeadingType.Footer)
                    .Appearance.Justification = LineJustification.Left
                End With

                .Font = New Font("Ariel", 11)
            End With
            consecutiveLineNumber = 0

            ' display the report
            Using dlg As New PrintPreviewDialog
                dlg.Document = autoPageReport
                dlg.WindowState = FormWindowState.Maximized
                dlg.ShowDialog()
            End Using

        End Using
    End Sub

    Private Sub autoPageReport_PrintPageBodyStart(ByVal sender As Object,
                                                  ByVal e As isr.Drawing.Reporting.ReportPageEventArgs) Handles autoPageReport.PrintPageBodyStarted

        While consecutiveLineNumber < linesToPrint
            ' increment and print line number
            consecutiveLineNumber += 1
            e.WriteLine(String.Format(Globalization.CultureInfo.CurrentCulture,
                                      "Consecutive Line Number: {0:0000}", consecutiveLineNumber))

            ' make sure we don't run off the end of the page
            If e.EndOfPage Then Exit While

        End While

        ' indicate whether we have more data to display
        If consecutiveLineNumber < linesToPrint Then
            e.HasMorePages = True
        End If

    End Sub

#End Region

#Region " REPORT FROM DATA SET "

    Private Shared Sub doReportFromDataSet()

        ' load employees data from pubs
        Dim dbConn As String = "data source=INEROTH;initial catalog=pubs;integrated security=SSPI"
        Dim query As String = "SELECT emp_id,FNAME,LNAME FROM employee"
        Using da As New System.Data.SqlClient.SqlDataAdapter(query, dbConn)
            Using ds As New DataSet
                ds.Locale = Globalization.CultureInfo.CurrentCulture
                da.Fill(ds)
                ' initialize report
                Using report As New isr.Drawing.Reporting.ReportDocument
                    With report

                        With .AddHeading(Date.Now.ToString(Globalization.CultureInfo.CurrentCulture), HeadingType.SupHeader)
                            .Appearance.Justification = LineJustification.Left
                        End With

                        With .AddHeading("ISR Report Generator", HeadingType.SupHeader)
                            .Appearance.Justification = LineJustification.Right
                        End With

                        With .AddHeading("Pubs Employees", HeadingType.Header)
                            .Appearance.Font = New Font(.Appearance.Font, FontStyle.Bold)
                        End With

                        With .AddHeading("(c) 2010 Integrated Scientific Resources, Inc., Confidential", HeadingType.Footer)
                            .Appearance.Justification = LineJustification.Left
                        End With

                        .Font = New Font("Ariel", 10)

                        ' set the data source, using auto discover to find the columns
                        .AutoDiscover = True
                        .DataSource = ds

                        ' override the column names to be more human readable
                        .Columns(0).Name = "Employee id"
                        .Columns(1).Name = "First name"
                        .Columns(2).Name = "Last name"
                    End With

                    ' display the report
                    Using dlg As New PrintPreviewDialog
                        dlg.Document = report
                        dlg.WindowState = FormWindowState.Maximized
                        dlg.ShowDialog()
                    End Using
                End Using
            End Using

        End Using

    End Sub

#End Region

#Region " REPORT FROM OBJECTS "

    Private Shared Sub doReportFromObjectArrayList()

        ' load some data
        Dim data As New ArrayList
        data.Add(New Person("John", "Bloomington", "MN"))
        data.Add(New Person("Mary", "Bloomington", "IL"))
        data.Add(New Person("Aaron", "Minneapolis", "MN"))
        data.Add(New Person("Ben", "San Francisco", "CA"))

        ' initialize report
        Using report As New isr.Drawing.Reporting.ReportDocument

            With report

                With .AddHeading(Date.Now.ToString(Globalization.CultureInfo.CurrentCulture), HeadingType.SupHeader)
                    .Appearance.Justification = LineJustification.Left
                End With

                With .AddHeading("ISR Report Generator", HeadingType.SupHeader)
                    .Appearance.Justification = LineJustification.Right
                End With

                With .AddHeading("Report from Person Objects", HeadingType.Header)
                    .Appearance.Font = New Font(.Appearance.Font, FontStyle.Bold)
                End With

                With .AddHeading("(c) 2010 Integrated Scientific Resources, Inc., Confidential", HeadingType.Footer)
                    .Appearance.Justification = LineJustification.Left
                End With

                .Font = New Font("Ariel", 10)

                ' set the data source, using auto discover to find the columns
                .AutoDiscover = True
                .DataSource = data

            End With

            ' display the report
            Using dlg As New PrintPreviewDialog
                dlg.Document = report
                dlg.WindowState = FormWindowState.Maximized
                dlg.ShowDialog()
            End Using

        End Using
    End Sub

#End Region

#Region " REPORT WITH MULTIPLE SECTIONS "

    Private WithEvents multiPartReport As isr.Drawing.Reporting.ReportDocument
    Private objectArrayList As New ArrayList

    Private Sub doMultiPartReport()

        ' load some data into a collection of objects
        objectArrayList.Add(New Person("John", "Bloomington", "MN"))
        objectArrayList.Add(New Person("Mary", "Bloomington", "IL"))
        objectArrayList.Add(New Person("Aaron", "Minneapolis", "MN"))
        objectArrayList.Add(New Person("Ben", "San Francisco", "CA"))

        ' initialize report to report against DataSet
        Using multiPartReport As New isr.Drawing.Reporting.ReportDocument
            Me.multiPartReport = multiPartReport
            With multiPartReport

                With .AddHeading(Date.Now.ToString(Globalization.CultureInfo.CurrentCulture), HeadingType.SupHeader)
                    .Appearance.Justification = LineJustification.Left
                End With

                With .AddHeading("ISR Report Generator", HeadingType.SupHeader)
                    .Appearance.Justification = LineJustification.Right
                End With

                With .AddHeading("Multi-Section Report", HeadingType.Header)
                    .Appearance.Font = New Font(.Appearance.Font, FontStyle.Bold)
                End With

                With .AddHeading("(c) 2010 Integrated Scientific Resources, Inc., Confidential", HeadingType.Footer)
                    .Appearance.Justification = LineJustification.Left
                End With
                .Font = New Font("Ariel", 10)

                ' set the data source, using auto discover to find the columns
                .AutoDiscover = False

                ' start with the manual portion of the report section 1.
                .Section = 1

            End With

            ' display the report
            Using dlg As New PrintPreviewDialog
                dlg.Document = multiPartReport
                dlg.WindowState = FormWindowState.Maximized
                dlg.ShowDialog()
            End Using

        End Using
    End Sub

    Private Sub doReportFromultiPartReportDataSourcesb()

        ' load authors data from pubs
        Dim dbConn As String = "data source=INEROTH;initial catalog=pubs;integrated security=SSPI"
        Using da As New System.Data.SqlClient.SqlDataAdapter("SELECT emp_id,FNAME,LNAME FROM employee", dbConn)
            Using ds As New DataSet
                ds.Locale = Globalization.CultureInfo.CurrentCulture
                da.Fill(ds)
                ' load some data into a collection of objects
                objectArrayList.Add(New Person("John", "Bloomington", "MN"))
                objectArrayList.Add(New Person("Mary", "Bloomington", "IL"))
                objectArrayList.Add(New Person("Aaron", "Minneapolis", "MN"))
                objectArrayList.Add(New Person("Ben", "San Francisco", "CA"))

                ' initialize report to report against DataSet
                Using multiPartReport As New isr.Drawing.Reporting.ReportDocument
                    Me.multiPartReport = multiPartReport
                    With multiPartReport

                        With .AddHeading(Date.Now.ToString(Globalization.CultureInfo.CurrentCulture), HeadingType.SupHeader)
                            .Appearance.Justification = LineJustification.Left
                        End With

                        Me._supHeaderRight = .AddHeading("ISR Report Generator", HeadingType.SupHeader)
                        With Me._supHeaderRight
                            .Appearance.Justification = LineJustification.Right
                        End With

                        With .AddHeading("Multi-Part Report", HeadingType.Header)
                            .Appearance.Font = New Font(.Appearance.Font, FontStyle.Bold)
                        End With

                        With .AddHeading("(c) 2010 Integrated Scientific Resources, Inc., Confidential", HeadingType.Footer)
                            .Appearance.Justification = LineJustification.Left
                        End With

                        .Font = New Font("Ariel", 10)

                        ' set the data source, using auto discover to find the columns
                        .AutoDiscover = True
                        .DataSource = ds
                    End With
                    ' display the report
                    Using dlg As New PrintPreviewDialog
                        dlg.Document = multiPartReport
                        dlg.WindowState = FormWindowState.Maximized
                        dlg.ShowDialog()
                    End Using
                End Using

            End Using
        End Using


    End Sub

    ''' <summary>This event is raised for each page immediately after the header for 
    ''' the page has been printed. The cursor is on the first line of the report body.</summary>
    ''' <remarks>Use this method to print the body of the report.</remarks>
    Private Sub multiPartReport_PrintPageBodyStart(ByVal sender As Object,
                                                   ByVal e As isr.Drawing.Reporting.ReportPageEventArgs) Handles multiPartReport.PrintPageBodyStarted

        If multiPartReport.Section = 1 Then

            ' if manual portion, do the manual part.
            With e
                If e.PageNumber = 1 Then

                    .WriteLine("Welcome to our report.")
                    .WriteLine()
                    .Write("We can print some text, and ")
                    .WriteLine("then put some more text on the same line.")
                    .WriteLine("This works much like writing to the console.")
                    .WriteLine()
                    .WriteLine()
                    .Write("We can left-justify", LineJustification.Left)
                    .Write("We can also center text", LineJustification.Centered)
                    .WriteLine("And we can right-justify", LineJustification.Right)
                    .WriteLine()
                    .WriteLine()
                    .WriteLine("Much simpler than doing this all by hand.")
                    .HasMorePages = True

                Else

                    .WriteLine("This is page 2.")
                    .WriteLine("Just to show that we handle multiple pages.")
                    .HasMorePages = False

                End If

            End With

        End If

    End Sub

    Private Sub multiPartReport_ReportEnd(ByVal sender As Object, ByVal e As isr.Drawing.Reporting.ReportPageEventArgs) Handles multiPartReport.ReportEnded

        ' if we just reported the DataSet we don't want the 
        ' report to end, so override to also report
        ' the objects
        If TypeOf multiPartReport.DataSource Is DataSet Then

            ' indicate there ARE more pages
            e.HasMorePages = True

            ' Date.Now set the data source to our collection
            multiPartReport.DataSource = objectArrayList

            ' set the data source, using auto discover to find the columns
            multiPartReport.AutoDiscover = True

            ' change the subtitle to indicate the new type of data
            Me._supHeaderRight.Appearance.Text = "Person Objects"

            ' turn on the second section of the report
            multiPartReport.Section = 2

        End If

    End Sub

#End Region

#Region " REPORT WITH MULTIPLE DATA SETS "

    Private WithEvents multiDataSetsReport As isr.Drawing.Reporting.ReportDocument

    Dim _supHeaderRight As Heading
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
    Private Sub doMultiDataReport()

        ' load authors data from pubs
        Dim dbConn As String = "data source=INEROTH;initial catalog=pubs;integrated security=SSPI"
        Using da As New System.Data.SqlClient.SqlDataAdapter("SELECT emp_id,FNAME,LNAME FROM employee", dbConn)
            Using ds As New DataSet
                ds.Locale = Globalization.CultureInfo.CurrentCulture
                da.Fill(ds)

                ' load some data into a collection of objects
                objectArrayList.Add(New Person("John", "Bloomington", "MN"))
                objectArrayList.Add(New Person("Mary", "Bloomington", "IL"))
                objectArrayList.Add(New Person("Aaron", "Minneapolis", "MN"))
                objectArrayList.Add(New Person("Ben", "San Francisco", "CA"))

                ' initialize report to report against DataSet
                Using multiDataSetsReport As New isr.Drawing.Reporting.ReportDocument
                    Me.multiDataSetsReport = multiDataSetsReport
                    With multiDataSetsReport

                        With .AddHeading(Date.Now.ToString(Globalization.CultureInfo.CurrentCulture), HeadingType.SupHeader)
                            .Appearance.Justification = LineJustification.Left
                        End With

                        Me._supHeaderRight = .AddHeading("ISR Report Generator", HeadingType.SupHeader)
                        With Me._supHeaderRight
                            .Appearance.Justification = LineJustification.Right
                        End With

                        With .AddHeading("Report with multiple data sets", HeadingType.Header)
                            .Appearance.Font = New Font(.Appearance.Font, FontStyle.Bold)
                        End With

                        With .AddHeading("(c) 2010 Integrated Scientific Resources, Inc., Confidential", HeadingType.Footer)
                            .Appearance.Justification = LineJustification.Left
                        End With

                        .Font = New Font("Ariel", 10)

                        ' set the data source, using auto discover to find the columns
                        .AutoDiscover = True
                        .DataSource = ds
                    End With

                    ' display the report
                    Using dlg As New PrintPreviewDialog
                        dlg.Document = multiPartReport
                        dlg.WindowState = FormWindowState.Maximized
                        dlg.ShowDialog()
                    End Using
                End Using

            End Using
        End Using

    End Sub

    Private Sub multiDataSetsReport_ReportEnd(ByVal sender As Object,
                                              ByVal e As isr.Drawing.Reporting.ReportPageEventArgs) Handles multiDataSetsReport.ReportEnded

        ' if we just reported the DataSet we don't want the 
        ' report to end, so override to also report
        ' the objects
        If TypeOf multiDataSetsReport.DataSource Is DataSet Then

            ' indicate there ARE more pages
            e.HasMorePages = True

            ' Date.Now set the data source to our collection
            multiPartReport.DataSource = objectArrayList

            ' set the data source, using auto discover to find the columns
            multiPartReport.AutoDiscover = True

            ' change the subtitle to indicate the new type of data
            Me._supHeaderRight.Appearance.Text = "Person Objects"

        End If

    End Sub

#End Region

End Class
