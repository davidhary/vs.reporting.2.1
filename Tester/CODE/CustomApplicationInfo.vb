﻿Namespace My

    Partial Friend Class MyApplication

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = isr.Core.Diagnosis.TraceEventIds.IsrVisualizingReportingTester

        Public Const AssemblyTitle As String = "Drawing Reporting Tester"
        Public Const AssemblyDescription As String = "Drawing Reporting Tester"
        Public Const AssemblyProduct As String = "Drawing.Reporting.Library.Tester.2014"

    End Class

End Namespace

