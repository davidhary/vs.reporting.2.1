''' <summary>
''' This is a list of the possible text justification values used by the
''' <see cref="M:isr.Drawing.Printing.ReportPageEventArgs.Write(System.String,isr.Drawing.Reporting.LineJustification)" />
''' and
''' <see cref="M:isr.Drawing.Printing.ReportPageEventArgs.WriteLine(System.String,isr.Drawing.Reporting.LineJustification)" />
''' methods.
''' </summary>
Public Enum LineJustification
    ''' <summary>
    ''' The none
    ''' </summary>
    None
    ''' <summary>
    ''' The left
    ''' </summary>
    Left
    ''' <summary>
    ''' The centered
    ''' </summary>
    Centered
    ''' <summary>
    ''' The right
    ''' </summary>
    Right
End Enum

''' <summary>
''' The ReportPageEventArgs the type of the parameter provided by
''' the events raised from the <see cref="T:isr.Drawing.Printing.ReportDocument" />
''' object. This class includes methods to simplify the process of
''' rendering text output into each page of the report.
''' </summary>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class ReportPageEventArgs
    Inherits System.Drawing.Printing.PrintPageEventArgs

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' This is the main constructor method for this class.
    ''' </summary>
    ''' <param name="e">The <see cref="System.Drawing.Printing.PrintPageEventArgs" /> instance containing the event data.</param>
    ''' <param name="pageNumber">The page number.</param>
    ''' <param name="font">The font.</param>
    ''' <param name="brush">The brush.</param>
    ''' <param name="footerLines">The footer lines.</param>
    ''' <exception cref="System.ArgumentNullException">font</exception>
    Friend Sub New(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal pageNumber As Integer, ByVal font As System.Drawing.Font,
                   ByVal brush As System.Drawing.Brush, ByVal footerLines As Integer)

        MyBase.New(e.Graphics, e.MarginBounds, e.PageBounds, e.PageSettings)

        If font Is Nothing Then
            Throw New ArgumentNullException("font")
        End If
        If brush Is Nothing Then
            Throw New ArgumentNullException("brush")
        End If

        Me._pageNumber = pageNumber
        Me._font = font
        Me._Brush = brush
        PositionToStart()
        Me._FooterLines = footerLines
        Me._LineHeight = Convert.ToInt32(Me._font.GetHeight(MyBase.Graphics))
        Me._pageBottom = MyBase.MarginBounds.Bottom - Me._FooterLines * Me._LineHeight - Me._LineHeight

    End Sub

#End Region

#Region " METHODS "

    ''' <summary>
    ''' Returns True if the cursor's current location is beyond the bottom of
    ''' the page body. This doesn't mean we're into the bottom margin, but may
    ''' indicate that the cursor in the page's footer region.
    ''' </summary>
    ''' <returns>A Boolean indicating whether the cursor is past the end of the page.</returns>
    Public Function EndOfPage() As Boolean
        Return Me.CurrentY >= Me._pageBottom
    End Function

    ''' <summary>
    ''' Returns True if the cursor's current location after printing the text
    ''' will be beyond the bottom of the page body.
    ''' </summary>
    ''' <param name="text">Text to consider for printing</param>
    ''' <returns>A Boolean indicating whether the cursor is past the end of the page.</returns>
    Public Function EndOfPage(ByVal text As String) As Boolean
        If String.IsNullOrWhiteSpace(text) Then
            Return Me.CurrentY >= Me._pageBottom
        Else
            Return Me.CurrentY + MyBase.Graphics.MeasureString(text, Me._font).Height >= Me._pageBottom
        End If
    End Function

    ''' <summary>
    ''' Returns True if the cursor's current location after printing the text
    ''' will be beyond the bottom of the page body.
    ''' </summary>
    ''' <param name="text">Text to consider for printing</param>
    ''' <param name="layoutArea">The <see cref="System.Drawing.SizeF">layout area</see> where the
    ''' teat is to be rendered</param>
    ''' <returns>A Boolean indicating whether the cursor is past the end of the page.</returns>
    Public Function EndOfPage(ByVal text As String, ByVal layoutArea As System.Drawing.SizeF) As Boolean
        If String.IsNullOrWhiteSpace(text) Then
            Return Me.CurrentY >= Me._pageBottom
        Else
            Return Me.CurrentY + MyBase.Graphics.MeasureString(text, Me._font, layoutArea).Height >= Me._pageBottom
        End If
    End Function

    ''' <summary>
    ''' Returns the position for printing the text.
    ''' </summary>
    ''' <param name="text">The text to render.</param>
    ''' <param name="font">The font.</param>
    ''' <param name="justification">The <see cref="Reporting.LineJustification">justification</see>
    ''' for the text.</param>
    ''' <returns>System.Int32.</returns>
    ''' <exception cref="System.ArgumentNullException">font</exception>
    Private Function getPosition(ByVal text As String, ByVal font As Font, ByVal justification As Reporting.LineJustification) As Integer

        ' set current position to write based on justification
        Select Case justification

            Case LineJustification.None

                ' use current position
                Return Me.CurrentX

            Case Reporting.LineJustification.Left

                Return MyBase.MarginBounds.Left

            Case Reporting.LineJustification.Centered

                If String.IsNullOrWhiteSpace(text) Then
                    Return MyBase.MarginBounds.Left + Convert.ToInt32(MyBase.MarginBounds.Width / 2)
                Else
                    If font Is Nothing Then
                        Throw New ArgumentNullException("font")
                    End If
                    Return MyBase.MarginBounds.Left + Convert.ToInt32(MyBase.MarginBounds.Width / 2 - 
                      MyBase.Graphics.MeasureString(text, font).Width / 2)
                End If

            Case Reporting.LineJustification.Right

                If String.IsNullOrWhiteSpace(text) Then
                    Return Convert.ToInt32(MyBase.MarginBounds.Right)
                Else
                    If font Is Nothing Then
                        Throw New ArgumentNullException("font")
                    End If
                    Return Convert.ToInt32(MyBase.MarginBounds.Right - 
                        MyBase.Graphics.MeasureString(text, font).Width)
                End If

            Case Else

                Debug.Assert(Not Debugger.IsAttached, "Unhandled justification type")

        End Select

    End Function

    ''' <summary>
    ''' Returns the position for printing the text.
    ''' </summary>
    ''' <param name="text">The text to render.</param>
    ''' <param name="font">The font.</param>
    ''' <param name="justification">The <see cref="Reporting.LineJustification">justification</see>
    ''' for the text.</param>
    ''' <param name="layoutArea">The layout area <see cref="RectangleF" /></param>
    ''' <returns>System.Int32.</returns>
    Private Function getPosition(ByVal text As String, ByVal font As Font, ByVal justification As Reporting.LineJustification,
                                 ByVal layoutArea As RectangleF) As Integer

        ' set current position to write based on justification
        Select Case justification

            Case LineJustification.None

                ' use current position
                Return Me.CurrentX

            Case Reporting.LineJustification.Left

                Return Convert.ToInt32(layoutArea.X)

            Case Reporting.LineJustification.Centered

                If String.IsNullOrWhiteSpace(text) Then
                    Return Convert.ToInt32(layoutArea.Left + layoutArea.Width / 2)
                Else
                    Return Convert.ToInt32(layoutArea.Left + layoutArea.Width / 2) - 
                      Convert.ToInt32(MyBase.Graphics.MeasureString(text, font).Width / 2)
                End If

            Case Reporting.LineJustification.Right

                If String.IsNullOrWhiteSpace(text) Then
                    Return Convert.ToInt32(layoutArea.Right)
                Else
                    Return Convert.ToInt32(layoutArea.Right - 
                        MyBase.Graphics.MeasureString(text, font).Width)
                End If

            Case Else

                Debug.Assert(Not Debugger.IsAttached, "Unhandled justification type")

        End Select

    End Function

    ''' <summary>
    ''' Draws a horizontal line across the width of the page on the current
    ''' line. After the line is drawn the cursor is moved down one line and
    ''' to the left side of the page.
    ''' </summary>
    Public Sub HorizontalLine()

        Dim y As Integer = Me.CurrentY + Convert.ToInt32(Me._lineHeight / 2)

        MyBase.Graphics.DrawLine(System.Drawing.Pens.Black, MyBase.MarginBounds.Left, y, MyBase.MarginBounds.Right, y)
        WriteLine()

    End Sub

    ''' <summary>
    ''' Moves the cursor to the top left corner of the page.
    ''' </summary>
    Public Sub PositionToFooter()

        Me.CurrentX = MyBase.MarginBounds.Left
        Me.CurrentY = MyBase.MarginBounds.Bottom

    End Sub

    ''' <summary>
    ''' Moves the cursor to the top left corner of the page.
    ''' </summary>
    Public Sub PositionToHeader()

        Me.CurrentX = MyBase.MarginBounds.Left
        Me.CurrentY = MyBase.MarginBounds.Top \ 2

    End Sub

    ''' <summary>
    ''' Moves the cursor to the top left corner of the page.
    ''' </summary>
    Public Sub PositionToStart()

        Me.CurrentX = MyBase.MarginBounds.Left
        Me.CurrentY = MyBase.MarginBounds.Top

    End Sub

    ''' <summary>
    ''' Writes some text to the report starting at the current cursor location.
    ''' The cursor is moved to the right, but not down to the next line.
    ''' </summary>
    ''' <param name="text">The text to render.</param>
    Public Sub Write(ByVal text As String)

        If Not String.IsNullOrWhiteSpace(text) Then
            MyBase.Graphics.DrawString(text, Me._font, Me._brush, Me.CurrentX, Me.CurrentY)
            Me.CurrentX += Convert.ToInt32(MyBase.Graphics.MeasureString(text, Me._font).Width)
        End If

    End Sub

    ''' <summary>
    ''' Writes some text to the report starting at the current cursor location.
    ''' The cursor is moved to the right, but not down to the next line.
    ''' </summary>
    ''' <param name="text">The text to render.</param>
    ''' <param name="font">The <see cref="Font" /></param>
    ''' <param name="brush">The <see cref="System.Drawing.Brush">Brush</see></param>
    ''' <exception cref="System.ArgumentNullException">font</exception>
    Public Sub Write(ByVal text As String, ByVal font As Font, ByVal brush As Brush)

        If font Is Nothing Then
            Throw New ArgumentNullException("font")
        End If
        If brush Is Nothing Then
            Throw New ArgumentNullException("brush")
        End If

        If Not String.IsNullOrWhiteSpace(text) Then
            MyBase.Graphics.DrawString(text, font, brush, Me.CurrentX, Me.CurrentY)
            Me.CurrentX += Convert.ToInt32(MyBase.Graphics.MeasureString(text, Me._font).Width)
        End If

    End Sub

    ''' <summary>
    ''' Writes some text to the report starting at the current cursor location.
    ''' The cursor is moved to the right, but not down to the next line.
    ''' </summary>
    ''' <param name="text">The text to render.</param>
    ''' <param name="font">The <see cref="Font" /></param>
    ''' <param name="brush">The <see cref="System.Drawing.Brush">Brush</see></param>
    ''' <param name="layoutArea">The <see cref="SizeF">size</see> of the area
    ''' allocated for writing</param>
    ''' <exception cref="System.ArgumentNullException">font</exception>
    Public Sub Write(ByVal text As String, ByVal font As Font, ByVal brush As Brush, ByVal layoutArea As SizeF)

        If Not String.IsNullOrWhiteSpace(text) Then
            If font Is Nothing Then
                Throw New ArgumentNullException("font")
            End If
            If brush Is Nothing Then
                Throw New ArgumentNullException("brush")
            End If
            MyBase.Graphics.DrawString(text, font, brush, New RectangleF(Me.CurrentX, Me.CurrentY, layoutArea.Width, layoutArea.Height))
        End If

    End Sub

    ''' <summary>
    ''' Writes text to the report on the current line, but justified based on
    ''' the justification parameter value. The cursor is moved to the right, but not
    ''' down to the next line.
    ''' </summary>
    ''' <param name="text">The text to render.</param>
    ''' <param name="font">The <see cref="Font" /></param>
    ''' <param name="brush">The <see cref="System.Drawing.Brush">Brush</see></param>
    ''' <param name="justification">The <see cref="Reporting.LineJustification">justification</see>
    ''' for the text.</param>
    ''' <exception cref="System.ArgumentNullException">font</exception>
    Public Sub Write(ByVal text As String, ByVal font As Font, ByVal brush As Brush, ByVal justification As Reporting.LineJustification)

        If Not String.IsNullOrWhiteSpace(text) Then

            ' validate arguments
            If font Is Nothing Then
                Throw New ArgumentNullException("font")
            End If
            If brush Is Nothing Then
                Throw New ArgumentNullException("brush")
            End If

            Me.CurrentX = Me.getPosition(text, font, justification)

            ' write the text.
            Write(text, font, brush)

        End If

    End Sub

    ''' <summary>
    ''' Writes text to the report on the current line, but justified based on
    ''' the justification parameter value. The cursor is moved to the right, but not
    ''' down to the next line.
    ''' </summary>
    ''' <param name="text">The text to render.</param>
    ''' <param name="font">The <see cref="Font" /></param>
    ''' <param name="brush">The <see cref="System.Drawing.Brush">Brush</see></param>
    ''' <param name="justification">The <see cref="Reporting.LineJustification">justification</see>
    ''' for the text.</param>
    ''' <param name="layoutArea">The layout area <see cref="RectangleF" /></param>
    ''' <exception cref="System.ArgumentNullException">font</exception>
    Public Sub Write(ByVal text As String, ByVal font As Font, ByVal brush As Brush,
                     ByVal justification As Reporting.LineJustification, ByVal layoutArea As RectangleF)

        If Not String.IsNullOrWhiteSpace(text) Then

            ' validate arguments
            If font Is Nothing Then
                Throw New ArgumentNullException("font")
            End If
            If brush Is Nothing Then
                Throw New ArgumentNullException("brush")
            End If

            Me.CurrentX = Me.getPosition(text, font, justification, layoutArea)

            ' write the text.
            Write(text, font, brush, layoutArea.Size)

        End If

    End Sub

    ''' <summary>
    ''' Writes text to the report on the current line, but justified based on
    ''' the justification parameter value. The cursor is moved to the right, but not
    ''' down to the next line.
    ''' </summary>
    ''' <param name="text">The text to render.</param>
    ''' <param name="justification">The <see cref="Reporting.LineJustification">justification</see>
    ''' for the text.</param>
    Public Sub Write(ByVal text As String, ByVal justification As Reporting.LineJustification)

        If Not String.IsNullOrWhiteSpace(text) Then

            Me.CurrentX = Me.getPosition(text, Me._font, justification)

            ' write the text.
            Write(text)

        End If

    End Sub

    ''' <summary>
    ''' Writes text into a specific column within the report on
    ''' the current line. It uses a <see cref="T:isr.Drawing.Printing.ReportColumn" />
    ''' object to define the X position and width of the column. The cursor
    ''' is not moved by calling this method.
    ''' </summary>
    ''' <param name="text">The text to render into the column.</param>
    ''' <param name="column">The <see cref="T:isr.Drawing.Printing.ReportColumn" /> object defining this column.</param>
    ''' <exception cref="System.ArgumentNullException">column</exception>
    Public Sub WriteColumn(ByVal text As String, ByVal column As ReportColumn)

        If Not String.IsNullOrWhiteSpace(text) Then

            If column Is Nothing Then
                Throw New ArgumentNullException("column")
            End If
            Dim x As Integer = MyBase.MarginBounds.Left + column.Left
            MyBase.Graphics.FillRectangle(System.Drawing.Brushes.White,
                                          New System.Drawing.Rectangle(x - 5, Me.CurrentY, column.Width + 5, Me._LineHeight))
            MyBase.Graphics.DrawString(text, Me._font, Me._brush, x, Me.CurrentY)

        End If

    End Sub

    ''' <summary>
    ''' Moves the cursor down one line and to the left side of the page.
    ''' </summary>
    ''' <param name="verticalShift">The amount of vertical shift in 0.1 inches</param>
    Public Sub WriteLine(ByVal verticalShift As Integer)

        Me.CurrentX = MyBase.MarginBounds.Left
        Me.CurrentY += verticalShift

    End Sub

    ''' <summary>
    ''' Moves the cursor down one line and to the left side of the page.
    ''' </summary>
    Public Sub WriteLine()

        WriteLine(Me._lineHeight)

    End Sub

    ''' <summary>
    ''' Moves the cursor down one line and to the left side of the page.
    ''' </summary>
    ''' <param name="font">The <see cref="Font" /></param>
    ''' <exception cref="System.ArgumentNullException">font</exception>
    Public Sub WriteLine(ByVal font As Font)

        ' validate arguments
        If font Is Nothing Then
            Throw New ArgumentNullException("font")
        End If

        WriteLine(Convert.ToInt32(font.GetHeight(MyBase.Graphics)))

    End Sub

    ''' <summary>
    ''' Writes text to the report starting at the current cursor location and
    ''' then moves the cursor down one line and to the left side of the page.
    ''' </summary>
    ''' <param name="text">The text.</param>
    Public Sub WriteLine(ByVal text As String)

        If Not String.IsNullOrWhiteSpace(text) Then
            MyBase.Graphics.DrawString(text, Me._font, Me._brush, Me.CurrentX, Me.CurrentY)
            WriteLine()
        End If

    End Sub

    ''' <summary>
    ''' Writes text to the report on the current line, but justified based on
    ''' the justification parameter value. The cursor is moved down to the next line.
    ''' </summary>
    ''' <param name="text">The text.</param>
    ''' <param name="justification">The <see cref="Reporting.LineJustification">justification</see>
    ''' for the text.</param>
    Public Sub WriteLine(ByVal text As String, ByVal justification As Reporting.LineJustification)

        If Not String.IsNullOrWhiteSpace(text) Then

            ' get new position 
            Me.CurrentX = Me.getPosition(text, Me._font, justification)

            ' write the text
            Write(text)

            ' move to the next line
            WriteLine()

        End If

    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary>
    ''' gets or sets the brush.
    ''' </summary>
    ''' <value>The brush.</value>
    Public Property Brush() As System.Drawing.Brush

    ''' <summary>
    ''' Sets or returns the current X position (left to right) of the
    ''' cursor on the page.
    ''' </summary>
    ''' <value>The horizontal position of the cursor.</value>
    Public Property CurrentX() As Integer

    ''' <summary>
    ''' Sets or returns the current Y position (top to bottom) of the
    ''' cursor on the page.
    ''' </summary>
    ''' <value>The vertical position of the cursor.</value>
    Public Property CurrentY() As Integer

    ''' <summary>
    ''' The Me._font
    ''' </summary>
    Private _font As System.Drawing.Font
    ''' <summary>
    ''' Gets or sets the font.
    ''' </summary>
    ''' <value>The font.</value>
    ''' <exception cref="System.ArgumentNullException">value</exception>
    Public Property Font() As System.Drawing.Font
        Get
            Return Me._font
        End Get
        Set(ByVal value As System.Drawing.Font)
            If value Is Nothing Then
                Throw New ArgumentNullException("value")
            End If
            Me._font = value
            Me._lineHeight = Convert.ToInt32(Me._font.GetHeight(MyBase.Graphics))
        End Set
    End Property

    ''' <summary>
    ''' Sets or gets the number of footer lines.
    ''' </summary>
    ''' <value>The footer lines.</value>
    Public Property FooterLines() As Integer

    ''' <summary>
    ''' Sets or gets the line height.  It is reset when setting the font.
    ''' </summary>
    ''' <value>The height of the line.</value>
    Public Property LineHeight() As Integer

    ''' <summary>
    ''' Gets or sets the number of pages.  This must be set by the report calling method
    ''' </summary>
    ''' <value>The page count.</value>
    Public Property PageCount() As Integer

    ''' <summary>
    ''' The Me._page bottom
    ''' </summary>
    Private _pageBottom As Integer
    ''' <summary>
    ''' Returns the Y value corresponding to the bottom of the page
    ''' body. This is the position immediately above the start of the
    ''' page footer.
    ''' </summary>
    ''' <value>The Y value of the bottom of the page.</value>
    Public ReadOnly Property PageBottom() As Integer
        Get
            Return Me._pageBottom + Me._lineHeight
        End Get
    End Property

    ''' <summary>
    ''' The Me._page number
    ''' </summary>
    Private _pageNumber As Integer
    ''' <summary>
    ''' Returns the page number of the current page. This value is automatically
    ''' incremented as each new page is rendered.
    ''' </summary>
    ''' <value>The current page number.</value>
    Public ReadOnly Property PageNumber() As Integer
        Get
            Return Me._pageNumber
        End Get
    End Property

#End Region

End Class

