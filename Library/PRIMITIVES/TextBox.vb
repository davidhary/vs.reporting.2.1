''' <summary>
''' A class that represents a text object on the graph.  A list of
''' <see cref="TextBox" /> objects is maintained by the <see cref="TextBoxCollection" />
''' collection class.
''' </summary>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class TextBox
    Implements ICloneable, IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Constructs a <see cref="TextBox" /> with properties from the
    ''' <see cref="CaptionDefaults" /> and <see cref="BodyTextDefaults" /> classes.
    ''' </summary>
    ''' <param name="area">The layout area <see cref="RectangleF">Rectangle</see></param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Public Sub New(ByVal area As RectangleF)

        MyBase.new()

        Me._pageNumber = 1
        Me._caption = New TextAppearance(String.Empty, New Font(CaptionDefaults.Font, CaptionDefaults.Font.Style),
                                      CType(CaptionDefaults.Brush.Clone, Brush), CaptionDefaults.Justification)
        Me._bodyText = New TextAppearance(String.Empty, New Font(BodyTextDefaults.Font, BodyTextDefaults.Font.Style),
                                       CType(BodyTextDefaults.Brush.Clone, Brush), BodyTextDefaults.Justification)
        Me._Layout = New LayoutArea(area)

    End Sub

    ''' <summary>
    ''' The Copy Constructor
    ''' </summary>
    ''' <param name="model">The isr.Drawing.Reporting.TextBox object from which to copy</param>
    ''' <exception cref="System.ArgumentNullException">model</exception>
    Public Sub New(ByVal model As isr.Drawing.Reporting.TextBox)

        MyBase.new()
        If model Is Nothing Then
            Throw New ArgumentNullException("model")
        End If
        Me._bodyText = model._bodyText.Copy
        Me._caption = model._caption.Copy
        Me._Layout = model._Layout.Copy
        Me._statusMessage = model._statusMessage

    End Sub

    ''' <summary>
    ''' Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup.
    ''' </summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived
    ''' class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary>
    ''' Gets or sets the disposed status sentinel.
    ''' </summary>
    ''' <value><c>True</c> if disposed; otherwise, <c>False</c>.</value>
    Protected Property IsDisposed As Boolean

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; <c>False</c> if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If disposing Then

                If Not Me.IsDisposed Then

                    ' Free managed resources when explicitly called
                    If Me._bodyText IsNot Nothing Then
                        Me._bodyText.Dispose()
                        Me._bodyText = Nothing
                    End If
                    If Me._caption IsNot Nothing Then
                        Me._caption.Dispose()
                        Me._caption = Nothing
                    End If
                    If Me._Layout IsNot Nothing Then
                        Me._Layout.Dispose()
                        Me._Layout = Nothing
                    End If

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " METHODS "

    ''' <summary>Deep-copy clone routine</summary>
    ''' <returns>A new, independent copy of the isr.Drawing.Reporting.TextBox</returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary>Deep-copy clone routine</summary>
    ''' <returns>A new, independent copy of the isr.Drawing.Reporting.TextBox</returns>
    Public Function Copy() As isr.Drawing.Reporting.TextBox
        Return New isr.Drawing.Reporting.TextBox(Me)
    End Function

    ''' <summary>Writes the caption and body text into the <see cref="Graphics"/> device
    '''   specified by the <see cref="ReportPageEventArgs">report event arguments</see>.</summary>
    ''' <param name="e">Reference to the <see cref="ReportPageEventArgs">report event arguments</see>.</param>
    ''' <returns>The size of the text that was written.</returns>
    Public Function Write(ByVal e As ReportPageEventArgs) As SizeF

        ' validate argument.
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If

        Dim textHeight As Single = 0
        Dim textWidth As Single = 0

        ' exit if nothing to print
        If Me._pageNumber <> e.PageNumber Then
            Return New SizeF(textWidth, textHeight)
        End If

        e.CurrentX = Convert.ToInt32(Me._Layout.Area.X)
        e.CurrentY = Convert.ToInt32(Me._Layout.Area.Y)
        Dim captionSize As SizeF = Me._caption.Write(e, Me._Layout)
        ' clone the body
        Dim body As TextAppearance = Me._bodyText.Copy
        ' set justification
        body.Justification = LineJustification.Left
        ' add spaces to indent by size of current x
        body.Text = String.Empty
        Dim builder As New System.Text.StringBuilder
        Dim space As String = String.Empty.PadRight(1)
        Do Until body.MeasureString(e.Graphics).Width > captionSize.Width
            builder.Append(space)
            body.Text = builder.ToString & "."
        Loop
        builder.Append(space)
        builder.Append(space)
        builder.Append(Me._bodyText.Text)
        body.Text = builder.ToString
        Dim bodyTextSize As SizeF = body.Write(e, Me._Layout)
        textHeight = bodyTextSize.Height
        If captionSize.Height > textHeight Then
            textHeight = captionSize.Height
        End If
        textWidth = bodyTextSize.Width
        If captionSize.Width > textWidth Then
            textWidth = captionSize.Width
        End If
        Return New SizeF(textWidth, textHeight)

    End Function

#End Region

#Region " PROPERTIES "

    Private _bodyText As TextAppearance
    ''' <summary>Gets or sets a reference to the text box body of text</summary>
    ''' <value>A <see cref="isr.Drawing.Reporting.TextAppearance">Text Appearance</see> instance</value>
    Public Property BodyText() As TextAppearance
        Get
            Return Me._bodyText
        End Get
        Set(ByVal value As TextAppearance)
            Me._bodyText = value
        End Set
    End Property

    Private _caption As TextAppearance
    ''' <summary>Gets or sets a reference to the text box caption</summary>
    ''' <value>A <see cref="isr.Drawing.Reporting.TextAppearance">Text Appearance</see> instance</value>
    Public Property Caption() As TextAppearance
        Get
            Return Me._caption
        End Get
        Set(ByVal value As TextAppearance)
            Me._caption = value
        End Set
    End Property

    ''' <summary>
    ''' The location of the <see cref="TextBox"/>.</summary>
    Public Property Layout() As isr.Drawing.Reporting.LayoutArea

    Private _pageNumber As Integer = 1
    ''' <summary>Gets or sets the text box page number.</summary>
    Public Property PageNumber() As Integer
        Get
            Return Me._pageNumber
        End Get
        Set(ByVal value As Integer)
            Me._pageNumber = value
        End Set
    End Property

    Private _statusMessage As String
    ''' <summary>Gets or sets the status message.</summary>
    ''' <value>A System.String value.</value>
    Public Property StatusMessage() As String
        Get
            Return Me._statusMessage
        End Get
        Set(ByVal value As String)
            Me._statusMessage = value
        End Set
    End Property

#End Region

End Class

#Region " DEFAULTS "

''' <summary>A simple subclass of the <see cref="TextBox"/> class that defines the
'''   default property values for the <see cref="TextBox"/> caption.</summary>
Public NotInheritable Class CaptionDefaults

    ''' <summary>A private constructor to prevent the compiler from generating a 
    '''   default constructor.</summary>
    Private Sub New()
    End Sub

    Private Shared _brush As System.Drawing.Brush = System.Drawing.Brushes.Black
    ''' <summary>Gets or sets the default <see cref="System.Drawing.Brush">Brush</see> that will be 
    '''   used to render the text box caption. This defaults to a solid black brush.</summary>
    ''' <value>A <see cref="System.Drawing.Brush">Brush</see> object</value>
    Public Shared Property Brush() As System.Drawing.Brush
        Get
            Return CaptionDefaults._brush
        End Get
        Set(ByVal value As System.Drawing.Brush)
            CaptionDefaults._brush = value
        End Set
    End Property

    ''' <summary>Gets or sets the default <see cref="System.Drawing.Font">Font</see>
    '''   of the <see cref="TextBox"/> caption</summary>
    Private Shared _font As Font = New Font("Arial", 10, FontStyle.Regular Or FontStyle.Bold)
    Public Shared Property Font() As Font
        Get
            Return CaptionDefaults._font
        End Get
        Set(ByVal value As Font)
            CaptionDefaults._font = value
        End Set
    End Property

    Private Shared _justification As LineJustification = LineJustification.Left
    ''' <summary>Gets or sets the default <see cref="Reporting.LineJustification">justification</see>
    '''   of the TextBox caption.</summary>
    ''' <value>A <see cref="Reporting.LineJustification">justification</see> value.</value>
    Public Shared Property Justification() As Reporting.LineJustification
        Get
            Return CaptionDefaults._justification
        End Get
        Set(ByVal value As Reporting.LineJustification)
            CaptionDefaults._justification = value
        End Set
    End Property

End Class

''' <summary>A simple subclass of the <see cref="TextBox"/> class that defines the
'''   default property values for the <see cref="TextBox"/> body of text.</summary>
Public NotInheritable Class BodyTextDefaults

    ''' <summary>A private constructor to prevent the compiler from generating a 
    '''   default constructor.</summary>
    Private Sub New()
    End Sub

    Private Shared _brush As System.Drawing.Brush = System.Drawing.Brushes.Black
    ''' <summary>Gets or sets the default <see cref="System.Drawing.Brush">Brush</see> that will be 
    '''   used to render the text box body of text. This defaults to a solid black brush.</summary>
    ''' <value>A <see cref="System.Drawing.Brush">Brush</see> object</value>
    Public Shared Property Brush() As System.Drawing.Brush
        Get
            Return BodyTextDefaults._brush
        End Get
        Set(ByVal value As System.Drawing.Brush)
            BodyTextDefaults._brush = value
        End Set
    End Property

    ''' <summary>Gets or sets the default <see cref="System.Drawing.Font">Font</see>
    '''   of the <see cref="TextBox"/> body of text</summary>
    Private Shared _font As Font = New Font("Arial", 10, FontStyle.Regular)
    Public Shared Property Font() As Font
        Get
            Return BodyTextDefaults._font
        End Get
        Set(ByVal value As Font)
            BodyTextDefaults._font = value
        End Set
    End Property

    Private Shared _justification As LineJustification = LineJustification.None
    ''' <summary>Gets or sets the default <see cref="Reporting.LineJustification">justification</see>
    '''   of the TextBox body of text.</summary>
    ''' <value>A <see cref="Reporting.LineJustification">justification</see> value.</value>
    Public Shared Property Justification() As Reporting.LineJustification
        Get
            Return BodyTextDefaults._justification
        End Get
        Set(ByVal value As Reporting.LineJustification)
            BodyTextDefaults._justification = value
        End Set
    End Property

End Class

#End Region
