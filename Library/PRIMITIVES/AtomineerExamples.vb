﻿

''' <summary> Atomineer examples. </summary>
''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class AtomineerExamples
    Inherits System.Drawing.Printing.PrintDocument

    Protected Property IsDisposed As Boolean

    ''' <summary> Determines if we can disposed value. </summary>
    ''' <returns> true if it succeeds, false if it fails. </returns>
    ''' <remarks>
    ''' ATOMINEER IS NOT DOCUMENTING the about property.
    '''          </remarks>
    Protected Function DisposedValue() As Boolean
        Return IsDisposed
    End Function

End Class

Module Extensions

    ''' <summary> Low pass taper. </summary>
    ''' <remarks> NO EXCEPTIONS OR PARAMETER ARE DOCUMENTED</remarks>
    <Runtime.CompilerServices.Extension()>
    Public Sub LowPassTaper(ByVal spectrum As Single(),
                            ByVal frequency As Integer, ByVal transitionBankCount As Integer, ByVal toIndex As Integer)

        If spectrum Is Nothing Then
            Throw New System.ArgumentNullException("spectrum")
        End If

        If spectrum.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("spectrum", "The spectrum must have more than a one element.")
        End If

        If frequency - transitionBankCount \ 2 <= 0 Then
            Throw New ArgumentOutOfRangeException("frequency", frequency, "Filter frequency minus half the transition band must exceed zero.")
        End If

        If frequency - transitionBankCount \ 2 + transitionBankCount > toIndex Then
            Throw New ArgumentOutOfRangeException("toIndex", toIndex, "Maximum frequency must exceed the filter frequency plus half the transition band.")
        End If

        ' .....
    End Sub

End Module
