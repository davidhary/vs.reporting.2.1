''' <summary>
''' Defines a column into which text can be rendered on a line
''' of a table when the <see cref="T:.Printing.Report.ReportDocument" />
''' is bound to a data source.
''' </summary>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class ReportColumn

    Implements IDisposable, ICloneable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Constructs this class.
    ''' </summary>
    Public Sub New()

        ' instantiate the base class
        MyBase.New()

    End Sub

    ''' <summary>
    ''' Constructs this class.
    ''' </summary>
    ''' <param name="instanceName">Specifies the name of the instance.</param>
    ''' <remarks>Use this constructor to instantiate this class
    ''' and set the instance name, which is useful in tracing</remarks>
    Public Sub New(ByVal instanceName As String)

        ' instantiate the base class
        Me.New()
        Me._instanceName = instanceName

    End Sub

    ''' <summary>
    ''' The Copy Constructor
    ''' </summary>
    ''' <param name="model">The ReportColumn object from which to copy</param>
    ''' <exception cref="System.ArgumentNullException">model</exception>
    Public Sub New(ByVal model As ReportColumn)

        MyBase.New()
        If model Is Nothing Then
            Throw New ArgumentNullException("model")
        End If
        Me._field = model._field
        Me._instanceName = model._instanceName
        Me._left = model._left
        Me._name = model._name
        Me._width = model._width
        Me._statusMessage = model._statusMessage

    End Sub

    ''' <summary>
    ''' Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup.
    ''' </summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived
    ''' class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary>
    ''' Gets or sets the disposed status sentinel.
    ''' </summary>
    ''' <value><c>True</c> if disposed; otherwise, <c>False</c>.</value>
    Protected Property IsDisposed As Boolean

    ''' <summary>
    ''' Cleans up unmanaged or managed and unmanaged resources.
    ''' </summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged
    ''' resources; <c>False</c> if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    ''' its disposing parameter.  If True, the method has been called directly or
    ''' indirectly by a user's code--managed and unmanaged resources can be disposed.
    ''' If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference
    ''' other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If disposing Then

                If Not Me.IsDisposed = True

                    ' Free managed resources when explicitly called
                    Me._statusMessage = String.Empty
                    Me._instanceName = String.Empty

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " METHODS AND PROPERTIES "

    ''' <summary>
    ''' Deep-copy clone routine
    ''' </summary>
    ''' <returns>A new, independent copy of the Arrow</returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary>
    ''' Deep-copy clone routine
    ''' </summary>
    ''' <returns>A new, independent copy of the Arrow</returns>
    Public Function Copy() As ReportColumn
        Return New ReportColumn(Me)
    End Function

    ''' <summary>
    ''' Overrides ToString returning the instance name if not empty.
    ''' </summary>
    ''' <returns>A <see cref="System.String" /> that represents this instance.</returns>
    ''' <remarks>Use this method to return the instance name. If instance name is not set,
    ''' returns the base class ToString value.</remarks>
    Public Overrides Function ToString() As String
        If String.IsNullOrWhiteSpace(Me._instanceName) Then
            Return MyBase.ToString
        Else
            Return Me._instanceName
        End If
    End Function

    ''' <summary>
    ''' The Me._instance name
    ''' </summary>
    Private _instanceName As String = String.Empty
    ''' <summary>
    ''' Gets or sets the name given to an instance of this class.
    ''' </summary>
    ''' <value><c>InstanceName</c> is a String property.</value>
    Public Property InstanceName() As String
        Get
            Return Me.ToString
        End Get
        Set(ByVal value As String)
            Me._instanceName = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the status message.
    ''' </summary>
    ''' <value>A <see cref="System.String">String</see>.</value>
    Public Property StatusMessage() As String

#End Region

#Region " PROPERTIES "

    ''' <summary>
    ''' Defines the human-readable name of the column. This value
    ''' can be useful for generating descriptive headers.
    ''' </summary>
    ''' <value>The name.</value>
    Public Property Name() As String

    ''' <summary>
    ''' Contains the name of the field within the data source that
    ''' contains the data. This value is used to retrieve the data
    ''' value from the data source. It corresponds to the column
    ''' name in a DataTable, or a property name of an object.
    ''' </summary>
    ''' <value>The field.</value>
    Public Property Field() As String

    ''' <summary>
    ''' Defines the horizontal start location (X coordinate) of the
    ''' column. When text is written to the column by the
    ''' <see cref="M:isr.Drawing.Reporting.ReportPageEventArgs.WriteColumn(System.String,isr.Drawing.Printing.ReportColumn)" /> method
    ''' it is rendered starting at this horizontal location.
    ''' </summary>
    ''' <value>The left.</value>
    Public Property Left() As Integer

    ''' <summary>
    ''' Defines the width of the column. Before text is written to the
    ''' column by the
    ''' <see cref="M:isr.Drawing.Reporting.ReportPageEventArgs.WriteColumn(System.String,isr.Drawing.Printing.ReportColumn)" /> method
    ''' the column is filled with a white rectangle defined by the width
    ''' of the column. This helps prevent text from overwriting other
    ''' text within our columns.
    ''' </summary>
    ''' <value>The width.</value>
    Friend Property Width() As Integer

#End Region

#Region " PRIVATE  and  PROTECTED "

#End Region

End Class

