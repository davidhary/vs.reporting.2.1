''' <summary>
''' Contains a list of <see cref="TextBox" /> objects to display on the graph.
''' </summary>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <remarks>Declare <see cref="A:NotInheritable" /> so as to allow calling base methods in the constructor.</remarks>
Public NotInheritable Class TextBoxCollection
    Inherits System.Collections.ObjectModel.Collection(Of TextBox)
    Implements ICloneable

    ''' <summary>Default constructor for the <see cref="TextBoxCollection"/> collection class.</summary>
    Public Sub New()
        MyBase.new()
    End Sub

    ''' <summary>The Copy Constructor</summary>
    ''' <param name="model">The TextBoxCollection object from which to copy</param>
    Public Sub New(ByVal model As TextBoxCollection)
        MyBase.new()
        If model Is Nothing Then
            Throw New ArgumentNullException("model")
        End If
        Dim item As isr.Drawing.Reporting.TextBox
        For Each item In model
            Me.Add(New isr.Drawing.Reporting.TextBox(item))
        Next item
    End Sub

    ''' <summary>Deep-copy clone routine</summary>
    ''' <returns>A new, independent copy of the TextBoxCollection</returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary>Deep-copy clone routine</summary>
    ''' <returns>A new, independent copy of the TextBoxCollection</returns>
    Public Function Copy() As TextBoxCollection
        Return New TextBoxCollection(Me)
    End Function

    ''' <summary>Writes the <see cref="TextBox">text box</see> using the <see cref="Graphics"/> device
    '''   specified by the <see cref="ReportPageEventArgs">report event arguments</see>.</summary>
    ''' <param name="e">Reference to the <see cref="ReportPageEventArgs">report event arguments</see>.</param>
    Public Sub Write(ByVal e As ReportPageEventArgs)

        ' validate argument.
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If

        For Each textBox As TextBox In Me
            textBox.Write(e)
        Next textBox

    End Sub

End Class
