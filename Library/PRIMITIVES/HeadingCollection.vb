''' <summary>
''' Contains a list of <see cref="Heading" /> objects to print in the report.
''' </summary>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <remarks>Declare <see cref="A:NotInheritable" /> so as to allow calling base methods in the constructor.</remarks>
Public NotInheritable Class HeadingCollection
    Inherits System.Collections.ObjectModel.Collection(Of Heading)
    Implements ICloneable

    ''' <summary>
    ''' Default constructor for the <see cref="HeadingCollection">Heading Collection</see> class.
    ''' </summary>
    Public Sub New()
        MyBase.new()
    End Sub

    ''' <summary>
    ''' The Copy Constructor
    ''' </summary>
    ''' <param name="model">The <see cref="HeadingCollection">Heading Collection</see>
    ''' from which to copy</param>
    ''' <exception cref="System.ArgumentNullException">model</exception>
    Public Sub New(ByVal model As HeadingCollection)
        MyBase.new()
        If model Is Nothing Then
            Throw New ArgumentNullException("model")
        End If
        Dim item As isr.Drawing.Reporting.Heading
        For Each item In model
            Me.Add(New isr.Drawing.Reporting.Heading(item))
        Next item
    End Sub

    ''' <summary>
    ''' Deep-copy clone routine
    ''' </summary>
    ''' <returns>A new, independent copy of the <see cref="HeadingCollection">Heading Collection</see></returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary>
    ''' Deep-copy clone routine
    ''' </summary>
    ''' <returns>A new, independent copy of the <see cref="HeadingCollection">Heading Collection</see></returns>
    Public Function Copy() As HeadingCollection
        Return New HeadingCollection(Me)
    End Function

    ''' <summary>
    ''' Writes the <see cref="Heading">Heading</see> using the <see cref="Graphics" /> device
    ''' specified by the <see cref="ReportPageEventArgs">report event arguments</see>.
    ''' </summary>
    ''' <param name="e">Reference to the <see cref="ReportPageEventArgs">report event arguments</see>.</param>
    ''' <param name="headingType">The <see cref="HeadingType">Heading type</see> for the
    ''' <see cref="Heading" />..</param>
    ''' <returns>The line height of the heading.</returns>
    ''' <exception cref="System.ArgumentNullException">e</exception>
    Public Function Write(ByVal e As ReportPageEventArgs, ByVal headingType As HeadingType) As Integer

        ' validate argument.
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If

        Dim lineHeight As Integer = 0
        For Each Heading As Heading In Me
            If Heading.Visible Then
                If Heading.HeadingType = headingType Then
                    Dim i As Integer = Heading.Write(e)
                    If i > lineHeight Then
                        lineHeight = i
                    End If
                End If
            End If
        Next Heading
        Return lineHeight

    End Function

End Class
