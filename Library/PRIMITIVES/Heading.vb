''' <summary>
''' Handles specification and drawing of report headings.
''' </summary>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class Heading

    Implements ICloneable, IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Default constructor for <see cref="ReportDocument">report</see> title sets all title properties
    ''' to default values as defined in the <see cref="HeaderDefaults" /> class.
    ''' </summary>
    ''' <param name="type">The heading <see cref="HeadingType">type</see></param>
    Public Sub New(ByVal type As HeadingType)

        MyBase.new()

        Me._Appearance = New TextAppearance(String.Empty,
                                         CType(TitleDefaults.Font.Clone, Font), CType(TitleDefaults.Brush.Clone, System.Drawing.Brush),
                                         TitleDefaults.Justification)
        Me._HeadingType = HeadingType.Title
        Me._Visible = TitleDefaults.Visible
        Me._StatusMessage = String.Empty

        Me._HeadingType = type
        Select Case Me._headingType

            Case HeadingType.Footer, HeadingType.SubFooter, HeadingType.SupFooter

                Me._Visible = FooterDefaults.Visible
                Me._appearance.Brush = CType(FooterDefaults.Brush.Clone, System.Drawing.Brush)
                Me._appearance.Font = CType(FooterDefaults.Font.Clone, Font)
                Me._appearance.Justification = FooterDefaults.Justification

            Case HeadingType.Header, HeadingType.SubHeader, HeadingType.SupHeader

                Me._Visible = HeaderDefaults.Visible
                Me._appearance.Brush = CType(HeaderDefaults.Brush.Clone, System.Drawing.Brush)
                Me._appearance.Font = CType(HeaderDefaults.Font.Clone, Font)
                Me._appearance.Justification = HeaderDefaults.Justification

            Case HeadingType.Title

                Me._Visible = TitleDefaults.Visible
                Me._appearance.Brush = CType(TitleDefaults.Brush.Clone, System.Drawing.Brush)
                Me._appearance.Font = CType(TitleDefaults.Font.Clone, Font)
                Me._appearance.Justification = TitleDefaults.Justification

            Case HeadingType.Subtitle

                Me._Visible = SubtitleDefaults.Visible
                Me._appearance.Brush = CType(SubtitleDefaults.Brush.Clone, System.Drawing.Brush)
                Me._appearance.Font = CType(SubtitleDefaults.Font.Clone, Font)
                Me._appearance.Justification = SubtitleDefaults.Justification

            Case Else

                Debug.Assert(Not Debugger.IsAttached, "unhandled heading type")

        End Select

    End Sub

    ''' <summary>
    ''' Default constructor for <see cref="ReportDocument">report</see> Title sets all title properties
    ''' to default values as defined in the <see cref="HeaderDefaults" /> class.
    ''' </summary>
    ''' <param name="caption">The Heading caption</param>
    ''' <param name="type">The heading <see cref="HeadingType">type</see></param>
    Public Sub New(ByVal caption As String, ByVal type As HeadingType)

        Me.new(type)
        If String.IsNullOrWhiteSpace(caption) Then
            caption = String.Empty
        End If
        Me._appearance.Text = caption

    End Sub

    ''' <summary>
    ''' The Copy Constructor
    ''' </summary>
    ''' <param name="model">The Title object from which to copy</param>
    ''' <exception cref="System.ArgumentNullException">model</exception>
    Public Sub New(ByVal model As Heading)

        MyBase.new()
        If model Is Nothing Then
            Throw New ArgumentNullException("model")
        End If
        Me._appearance = model._appearance.Copy
        Me._headingType = model._headingType
        Me._visible = model._visible
        Me._statusMessage = model._statusMessage

    End Sub

    ''' <summary>
    ''' Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup.
    ''' </summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived
    ''' class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary>
    ''' Gets or sets the disposed status sentinel.
    ''' </summary>
    ''' <value><c>True</c> if disposed; otherwise, <c>False</c>.</value>
    Protected Property IsDisposed As Boolean

    ''' <summary>
    ''' Cleans up unmanaged or managed and unmanaged resources.
    ''' </summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged
    ''' resources; <c>False</c> if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    ''' its disposing parameter.  If True, the method has been called directly or
    ''' indirectly by a user's code--managed and unmanaged resources can be disposed.
    ''' If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference
    ''' other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try


            If disposing Then

                If Not Me.IsDisposed = True

                    If Me._appearance IsNot Nothing Then
                        Me._appearance.Dispose()
                    End If

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " METHODS "

    ''' <summary>
    ''' Deep-copy clone routine
    ''' </summary>
    ''' <returns>A new, independent copy of the Title</returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary>
    ''' Deep-copy clone routine
    ''' </summary>
    ''' <returns>A new, independent copy of Title</returns>
    Public Function Copy() As Heading
        Return New Heading(Me)
    End Function

    ''' <summary>
    ''' Get a <see cref="System.Drawing.SizeF">Size</see> structure representing the width and height
    ''' of the title caption based on the scaled font size.
    ''' </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    ''' <see cref="PaintEventArgs.Graphics" /> of the <see cref="M:Paint" /> method. </param>
    ''' <returns> Scaled <see cref="System.Drawing.SizeF">Size</see> dimensions in pixels,. </returns>
    Public Function MeasureString(ByVal graphicsDevice As System.Drawing.Graphics) As SizeF
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException("graphicsDevice")
        End If
        Return graphicsDevice.MeasureString(Me._Appearance.Text, Me._Appearance.Font)
    End Function

    ''' <summary>
    ''' Return the title caption
    ''' </summary>
    ''' <returns>A <see cref="System.String" /> value</returns>
    Public Overloads Overrides Function ToString() As String

        Return Me._appearance.Text

    End Function

    ''' <summary>
    ''' Writes the <see cref="Heading">Heading</see> using the <see cref="Graphics" /> device
    ''' specified by the <see cref="ReportPageEventArgs">report event arguments</see>.
    ''' </summary>
    ''' <param name="e">Reference to the <see cref="ReportPageEventArgs">report event arguments</see>.</param>
    ''' <returns>The line height.</returns>
    ''' <exception cref="System.ArgumentNullException">e</exception>
    Public Function Write(ByVal e As ReportPageEventArgs) As Integer

        ' validate argument.
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If

        Dim headerAppearance As TextAppearance = Me._appearance.Copy()
        If Me._isDate Then
            If Me._appearance.Text.Length > 0 Then
                headerAppearance.Text = String.Format(Globalization.CultureInfo.CurrentCulture, Me._appearance.Text, Date.Now.ToShortDateString)
            Else
                headerAppearance.Text = Date.Now.ToShortDateString()
            End If
        ElseIf Me._isPage Then
            If Me._isPageCount Then
                If Me._appearance.Text.Length > 0 Then
                    headerAppearance.Text = String.Format(Globalization.CultureInfo.CurrentCulture, Me._appearance.Text, e.PageNumber, e.PageCount)
                Else
                    headerAppearance.Text = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                          "Page {0} of {1}", e.PageNumber, e.PageCount)
                End If
            Else
                If Me._appearance.Text.Length > 0 Then
                    headerAppearance.Text = String.Format(Globalization.CultureInfo.CurrentCulture, Me._appearance.Text, e.PageNumber)
                Else
                    headerAppearance.Text = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                          "Page {0}", e.PageNumber)
                End If
            End If
        End If
        Return headerAppearance.Write(e)

    End Function

    ''' <summary>
    ''' Returns the heading line height
    ''' </summary>
    ''' <param name="graphicsDevice">References to the graphics device</param>
    ''' <returns>System.Int32.</returns>
    ''' <exception cref="System.ArgumentNullException">graphicsDevice</exception>
    Public Function LineHeight(ByVal graphicsDevice As Graphics) As Integer
        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException("graphicsDevice")
        End If
        Return Convert.ToInt32(Me._appearance.Font.GetHeight(graphicsDevice))
    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary>
    ''' Gets or sets the text appearance for the heading.
    ''' </summary>
    ''' <value>A <see cref="isr.Drawing.Reporting.TextAppearance">Text Appearance</see></value>
    Public Property Appearance() As TextAppearance

    ''' <summary>
    ''' Determines if the <see cref="Heading" /> will be show the date
    ''' </summary>
    ''' <value>A <see cref="System.Boolean">Boolean</see></value>
    Public Property IsDate() As Boolean

    ''' <summary>
    ''' Determines if the <see cref="Heading" /> will show the page
    ''' </summary>
    ''' <value>A <see cref="System.Boolean">Boolean</see></value>
    Public Property IsPage() As Boolean

    ''' <summary>
    ''' Determines if the <see cref="Heading" /> will show the page count as page x of N
    ''' </summary>
    ''' <value>A <see cref="System.Boolean">Boolean</see></value>
    Public Property IsPageCount() As Boolean
    ''' <summary>
    ''' Determines if the <see cref="Heading" /> will be drawn
    ''' </summary>
    ''' <value>A <see cref="System.Boolean">Boolean</see></value>
    Public Property Visible() As Boolean

    ''' <summary>
    ''' Gets or sets the <see cref="HeadingType">Heading type</see> for the
    ''' <see cref="Heading" />.
    ''' </summary>
    ''' <value>A <see cref="System.Drawing.Color">Color</see></value>
    Public Property HeadingType() As HeadingType

    ''' <summary>
    ''' Gets or sets the status message.
    ''' </summary>
    ''' <value>A System.String value.</value>
    Public Property StatusMessage() As String

#End Region

End Class

#Region " DEFAULTS "

''' <summary>
''' A simple subclass of the <see cref="Heading" /> class that defines the
''' default property values for the <see cref="Heading" /> footer.
''' </summary>
Public NotInheritable Class FooterDefaults

    ''' <summary>
    ''' A private constructor to prevent the compiler from generating a
    ''' default constructor.
    ''' </summary>
    Private Sub New()
    End Sub

    ''' <summary>
    ''' The Me._brush
    ''' </summary>
    Private Shared _brush As System.Drawing.Brush = System.Drawing.Brushes.DarkGray
    ''' <summary>
    ''' Gets or sets the default <see cref="System.Drawing.Brush">Brush</see> that will be
    ''' used to render the heading footer. This defaults to a solid dark gray brush.
    ''' </summary>
    ''' <value>A <see cref="System.Drawing.Brush">Brush</see> object</value>
    Public Shared Property Brush() As System.Drawing.Brush
        Get
            Return FooterDefaults._brush
        End Get
        Set(ByVal value As System.Drawing.Brush)
            FooterDefaults._brush = value
        End Set
    End Property

    ''' <summary>
    ''' The Me._visible
    ''' </summary>
    Private Shared _visible As Boolean = True
    ''' <summary>
    ''' Gets or sets the default display mode for the <see cref="Heading" /> footer.
    ''' </summary>
    ''' <value><c>True</c> if visible; otherwise, <c>False</c>.</value>
    Public Shared Property Visible() As Boolean
        Get
            Return FooterDefaults._visible
        End Get
        Set(ByVal value As Boolean)
            FooterDefaults._visible = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the default <see cref="System.Drawing.Font">Font</see>
    ''' of the <see cref="Heading" /> footer
    ''' </summary>
    Private Shared _font As Font = New Font("Arial", 10, FontStyle.Regular)
    ''' <summary>
    ''' Gets or sets the font.
    ''' </summary>
    ''' <value>The font.</value>
    Public Shared Property Font() As Font
        Get
            Return FooterDefaults._font
        End Get
        Set(ByVal value As Font)
            FooterDefaults._font = value
        End Set
    End Property

    ''' <summary>
    ''' The Me._justification
    ''' </summary>
    Private Shared _justification As LineJustification = LineJustification.Left
    ''' <summary>
    ''' Gets or sets the default <see cref="Reporting.LineJustification">justification</see>
    ''' of the heading footer.
    ''' </summary>
    ''' <value>A <see cref="Reporting.LineJustification">justification</see> value.</value>
    Public Shared Property Justification() As Reporting.LineJustification
        Get
            Return FooterDefaults._justification
        End Get
        Set(ByVal value As Reporting.LineJustification)
            FooterDefaults._justification = value
        End Set
    End Property

End Class

''' <summary>
''' A simple subclass of the <see cref="Heading" /> class that defines the
''' default property values for the <see cref="Heading" /> header.
''' </summary>
Public NotInheritable Class HeaderDefaults

    ''' <summary>
    ''' A private constructor to prevent the compiler from generating a
    ''' default constructor.
    ''' </summary>
    Private Sub New()
    End Sub

    ''' <summary>
    ''' The Me._brush
    ''' </summary>
    Private Shared _brush As System.Drawing.Brush = System.Drawing.Brushes.DarkGray
    ''' <summary>
    ''' Gets or sets the default <see cref="System.Drawing.Brush">Brush</see> that will be
    ''' used to render the heading header. This defaults to a solid dark gray brush.
    ''' </summary>
    ''' <value>A <see cref="System.Drawing.Brush">Brush</see> object</value>
    Public Shared Property Brush() As System.Drawing.Brush
        Get
            Return HeaderDefaults._brush
        End Get
        Set(ByVal value As System.Drawing.Brush)
            HeaderDefaults._brush = value
        End Set
    End Property

    ''' <summary>
    ''' The Me._visible
    ''' </summary>
    Private Shared _visible As Boolean = True
    ''' <summary>
    ''' Gets or sets the default display mode for the <see cref="Heading" />
    ''' header.
    ''' </summary>
    ''' <value><c>True</c> if visible; otherwise, <c>False</c>.</value>
    Public Shared Property Visible() As Boolean
        Get
            Return HeaderDefaults._visible
        End Get
        Set(ByVal value As Boolean)
            HeaderDefaults._visible = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the default <see cref="System.Drawing.Font">Font</see>
    ''' of the <see cref="Heading" /> header
    ''' </summary>
    Private Shared _font As Font = New Font("Arial", 10, FontStyle.Regular)
    ''' <summary>
    ''' Gets or sets the font.
    ''' </summary>
    ''' <value>The font.</value>
    Public Shared Property Font() As Font
        Get
            Return HeaderDefaults._font
        End Get
        Set(ByVal value As Font)
            HeaderDefaults._font = value
        End Set
    End Property

    ''' <summary>
    ''' The Me._justification
    ''' </summary>
    Private Shared _justification As LineJustification = LineJustification.Left
    ''' <summary>
    ''' Gets or sets the default <see cref="Reporting.LineJustification">justification</see>
    ''' of the heading header.
    ''' </summary>
    ''' <value>A <see cref="Reporting.LineJustification">justification</see> value.</value>
    Public Shared Property Justification() As Reporting.LineJustification
        Get
            Return HeaderDefaults._justification
        End Get
        Set(ByVal value As Reporting.LineJustification)
            HeaderDefaults._justification = value
        End Set
    End Property

End Class

''' <summary>
''' A simple subclass of the <see cref="Heading" /> class that defines the
''' default property values for the <see cref="Heading" /> sub-title.
''' </summary>
Public NotInheritable Class SubtitleDefaults

    ''' <summary>
    ''' A private constructor to prevent the compiler from generating a
    ''' default constructor.
    ''' </summary>
    Private Sub New()
    End Sub

    ''' <summary>
    ''' The Me._brush
    ''' </summary>
    Private Shared _brush As System.Drawing.Brush = System.Drawing.Brushes.Black
    ''' <summary>
    ''' Gets or sets the default <see cref="System.Drawing.Brush">Brush</see> that will be
    ''' used to render the heading sub-title. This defaults to a solid black brush.
    ''' </summary>
    ''' <value>A <see cref="System.Drawing.Brush">Brush</see> object</value>
    Public Shared Property Brush() As System.Drawing.Brush
        Get
            Return SubtitleDefaults._brush
        End Get
        Set(ByVal value As System.Drawing.Brush)
            SubtitleDefaults._brush = value
        End Set
    End Property

    ''' <summary>
    ''' The Me._visible
    ''' </summary>
    Private Shared _visible As Boolean = True
    ''' <summary>
    ''' Gets or sets the default display mode for the <see cref="Heading" /> sub-title.
    ''' </summary>
    ''' <value><c>True</c> if visible; otherwise, <c>False</c>.</value>
    Public Shared Property Visible() As Boolean
        Get
            Return SubtitleDefaults._visible
        End Get
        Set(ByVal value As Boolean)
            SubtitleDefaults._visible = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the default <see cref="System.Drawing.Font">Font</see>
    ''' of the <see cref="Heading" /> sub-title
    ''' </summary>
    Private Shared _font As Font = New Font("Arial", 12, FontStyle.Regular Or FontStyle.Bold)
    ''' <summary>
    ''' Gets or sets the font.
    ''' </summary>
    ''' <value>The font.</value>
    Public Shared Property Font() As Font
        Get
            Return SubtitleDefaults._font
        End Get
        Set(ByVal value As Font)
            SubtitleDefaults._font = value
        End Set
    End Property

    ''' <summary>
    ''' The Me._justification
    ''' </summary>
    Private Shared _justification As LineJustification = LineJustification.Centered
    ''' <summary>
    ''' Gets or sets the default <see cref="Reporting.LineJustification">justification</see>
    ''' of the heading sub-title.
    ''' </summary>
    ''' <value>A <see cref="Reporting.LineJustification">justification</see> value.</value>
    Public Shared Property Justification() As Reporting.LineJustification
        Get
            Return SubtitleDefaults._justification
        End Get
        Set(ByVal value As Reporting.LineJustification)
            SubtitleDefaults._justification = value
        End Set
    End Property

End Class

''' <summary>
''' A simple subclass of the <see cref="Heading" /> class that defines the
''' default property values for the <see cref="Heading" /> title.
''' </summary>
Public NotInheritable Class TitleDefaults

    ''' <summary>
    ''' A private constructor to prevent the compiler from generating a
    ''' default constructor.
    ''' </summary>
    Private Sub New()
    End Sub

    ''' <summary>
    ''' The Me._brush
    ''' </summary>
    Private Shared _brush As System.Drawing.Brush = System.Drawing.Brushes.Black
    ''' <summary>
    ''' Gets or sets the default <see cref="System.Drawing.Brush">Brush</see> that will be
    ''' used to render the heading title. This defaults to a solid black brush.
    ''' </summary>
    ''' <value>A <see cref="System.Drawing.Brush">Brush</see> object</value>
    Public Shared Property Brush() As System.Drawing.Brush
        Get
            Return TitleDefaults._brush
        End Get
        Set(ByVal value As System.Drawing.Brush)
            TitleDefaults._brush = value
        End Set
    End Property

    ''' <summary>
    ''' The Me._visible
    ''' </summary>
    Private Shared _visible As Boolean = True
    ''' <summary>
    ''' Gets or sets the default display mode for the <see cref="Heading" />
    ''' title.
    ''' </summary>
    ''' <value><c>True</c> if visible; otherwise, <c>False</c>.</value>
    Public Shared Property Visible() As Boolean
        Get
            Return TitleDefaults._visible
        End Get
        Set(ByVal value As Boolean)
            TitleDefaults._visible = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the default <see cref="System.Drawing.Font">Font</see>
    ''' of the <see cref="Heading" /> title
    ''' </summary>
    Private Shared _font As Font = New Font("Arial", 16, FontStyle.Regular Or FontStyle.Bold)
    ''' <summary>
    ''' Gets or sets the font.
    ''' </summary>
    ''' <value>The font.</value>
    Public Shared Property Font() As Font
        Get
            Return TitleDefaults._font
        End Get
        Set(ByVal value As Font)
            TitleDefaults._font = value
        End Set
    End Property

    ''' <summary>
    ''' The Me._justification
    ''' </summary>
    Private Shared _justification As LineJustification = LineJustification.Centered
    ''' <summary>
    ''' Gets or sets the default <see cref="Reporting.LineJustification">justification</see>
    ''' of the heading title.
    ''' </summary>
    ''' <value>A <see cref="Reporting.LineJustification">justification</see> value.</value>
    Public Shared Property Justification() As Reporting.LineJustification
        Get
            Return TitleDefaults._justification
        End Get
        Set(ByVal value As Reporting.LineJustification)
            TitleDefaults._justification = value
        End Set
    End Property

End Class

#End Region

#Region " TYPES "

''' <summary>
''' Enumerates the type of report headings.
''' </summary>
Public Enum HeadingType

    ''' <summary>
    ''' A title to print on the first page
    ''' </summary>
    <System.ComponentModel.Description("Title")> Title

    ''' <summary>
    ''' A sub-title to print on the first page
    ''' </summary>
    <System.ComponentModel.Description("Subtitle")> Subtitle

    ''' <summary>
    ''' A header to print at the top of each page
    ''' </summary>
    <System.ComponentModel.Description("Header")> Header

    ''' <summary>
    ''' A sub-header to print at top each page below the header
    ''' </summary>
    <System.ComponentModel.Description("SubHeader")> SubHeader

    ''' <summary>
    ''' A super header to print at top each page above the header
    ''' </summary>
    <System.ComponentModel.Description("SupHeader")> SupHeader

    ''' <summary>
    ''' A footer to print at bottom of each page
    ''' </summary>
    <System.ComponentModel.Description("Footer")> Footer

    ''' <summary>
    ''' A sub-footer to print at bottom of each page below the footer
    ''' </summary>
    <System.ComponentModel.Description("SubFooter")> SubFooter

    ''' <summary>
    ''' A super footer to print at bottom of each page above the footer
    ''' </summary>
    <System.ComponentModel.Description("SupFooter")> SupFooter

End Enum

#End Region
