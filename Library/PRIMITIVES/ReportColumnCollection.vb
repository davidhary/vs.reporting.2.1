''' <summary>
''' Defines a strongly-typed collection that contains
''' <see cref="T:isr.Drawing.Printing.ReportColumn" /> objects.
''' </summary>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <remarks>Declare <see cref="A:NotInheritable" /> so as to allow calling base methods in the constructor.</remarks>
Public NotInheritable Class ReportColumnCollection
    Inherits System.Collections.ObjectModel.Collection(Of ReportColumn)
    Implements ICloneable

#Region " CUSTOM COLLECTION METHODS "

    ''' <summary>Default constructor for the collection class</summary>
    Public Sub New()
        MyBase.new()
    End Sub

    ''' <summary>The Copy Constructor</summary>
    ''' <param name="model">The ReportColumnCollection object from which to copy</param>
    Public Sub New(ByVal model As ReportColumnCollection)
        MyBase.new()
        If model Is Nothing Then
            Throw New ArgumentNullException("model")
        End If
        Dim item As ReportColumn
        For Each item In model
            MyBase.Add(New ReportColumn(item))
        Next item
    End Sub

    ''' <summary>Deep-copy clone routine</summary>
    ''' <returns>A new, independent copy of the ArrowCollection</returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary>Deep-copy clone routine</summary>
    ''' <returns>A new, independent copy of the ArrowCollection</returns>
    Public Function Copy() As ReportColumnCollection
        Return New ReportColumnCollection(Me)
    End Function

    ''' <summary>Adds a <see cref="T:isr.Drawing.Printing.ReportColumn" /> object
    ''' to the collection based on a field name. The Name and Field
    ''' of the column are set to the provided field name. The 
    ''' Left and Width values are 0 and must be set separately.</summary>
    ''' <param name="Field">The name of the data field.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Public Overloads Sub Add(ByVal field As String)
        If String.IsNullOrWhiteSpace(field) Then
            Throw New ArgumentNullException("field")
        End If
        Dim col As New ReportColumn
        col.Name = field
        col.Field = field
        Add(col)
    End Sub

    ''' <summary>Adds a <see cref="T:isr.Drawing.Printing.ReportColumn" /> object
    ''' to the collection based on a field name. The Name and Field
    ''' of the column are set to the provided field name. The 
    ''' Left value is set to the provided value. The Width value
    ''' is 0 and must be set separately.</summary>
    ''' <param name="Field">The name of the data field.</param>
    ''' <param name="Left">The X position of the column.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Public Overloads Sub Add(ByVal field As String, ByVal left As Integer)
        If String.IsNullOrWhiteSpace(field) Then
            Throw New ArgumentNullException("field")
        End If
        Dim col As New ReportColumn
        col.Name = field
        col.Field = field
        col.Left = left
        Add(col)
    End Sub

    ''' <summary>Adds a <see cref="T:isr.Drawing.Printing.ReportColumn" /> object
    ''' to the collection based on a field name. The Name and Field
    ''' of the column are set to the provided values. The 
    ''' Left value is set to the provided value. The Width value
    ''' is 0 and must be set separately.</summary>
    ''' <param name="Name">The human-readable column name.</param>
    ''' <param name="Field">The name of the data field.</param>
    ''' <param name="Left">The X position of the column.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Public Overloads Sub Add(ByVal name As String, ByVal field As String, ByVal left As Integer)
        If String.IsNullOrWhiteSpace(name) Then
            Throw New ArgumentNullException("name")
        End If
        If String.IsNullOrWhiteSpace(field) Then
            Throw New ArgumentNullException("field")
        End If
        Dim col As New ReportColumn
        col.Name = name
        col.Field = field
        col.Left = left
        Add(col)
    End Sub

    ''' <summary>Called by the data binding mechanism to automatically run
    ''' through all the columns defined by this collection and to
    ''' set their widths to evenly consume all the horizontal space
    ''' on a line.</summary>
    ''' <param name="Width">The total width of a printed line.</param>
    Friend Sub SetEvenSpacing(ByVal width As Integer)

        Dim space As Integer = Convert.ToInt32(width / MyBase.Count)
        Dim index As Integer

        For index = 0 To MyBase.Count - 1
            With CType(MyBase.Item(index), ReportColumn)
                .Left = space * index
                .Width = space
            End With
        Next

    End Sub

#End Region

End Class
